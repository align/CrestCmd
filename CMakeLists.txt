atlas_subdir(CrestCmd )

#External dependencies:
find_package( CURL )
find_package( nlohmann_json )
find_package( Boost )
find_package( CrestApi )

set(CXX_FILESYSTEM_LIBRARIES stdc++fs)


atlas_add_executable(crestCmd  
   SOURCES src/crestCmd.cxx CrestApi/CrestApi.h CrestApi/CrestApiExt.h
   LINK_LIBRARIES CrestApiLib nlohmann_json::nlohmann_json 
   INCLUDE_DIRS ${CURL_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
   LINK_LIBRARIES ${CURL_LIBRARIES} ${Boost_LIBRARIES} -lstdc++fs
   FileCatalog CrestApi )

atlas_add_executable(crestExport  
   SOURCES src/crestExport.cxx CrestApi/CrestApi.h CrestApi/CrestApiExt.h
   LINK_LIBRARIES CrestApiLib nlohmann_json::nlohmann_json 
   INCLUDE_DIRS ${CURL_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
   LINK_LIBRARIES ${CURL_LIBRARIES} ${Boost_LIBRARIES} -lstdc++fs
   FileCatalog CrestApi )

atlas_add_executable(crestImport  
   SOURCES src/crestImport.cxx CrestApi/CrestApi.h CrestApi/CrestApiExt.h
   LINK_LIBRARIES CrestApiLib nlohmann_json::nlohmann_json 
   INCLUDE_DIRS ${CURL_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
   LINK_LIBRARIES ${CURL_LIBRARIES} ${Boost_LIBRARIES} -lstdc++fs
   FileCatalog CrestApi )

# This utility creates a tag with N iovs and payloads. 
# It is used to debug crestExport and crestImport application.
atlas_add_executable(tag_creation  
   SOURCES src/tag_creation.cxx CrestApi/CrestApiExt.h CrestApi/picosha2.h
   LINK_LIBRARIES CrestApiLib nlohmann_json::nlohmann_json
   INCLUDE_DIRS ${CURL_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
   LINK_LIBRARIES ${CURL_LIBRARIES} ${Boost_LIBRARIES} -lstdc++fs
   FileCatalog CrestApi )
