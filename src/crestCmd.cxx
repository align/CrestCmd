/*!
   \file
   \brief Main file

   This file contains the command line client to the CREST Server.
   It uses the CREST C++ Client Library.
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <nlohmann/json.hpp>

//#include <CrestApi/CrestApi.h>
#include <CrestApi/CrestApiExt.h>
#include <filesystem>

#include <getopt.h>
#include <stdio.h>

#include<dirent.h>  // if dir exists (test)

using namespace Crest;
using namespace std;

std::string SURL = "http://mvg-pc-04.cern.ch:8090";
std::string SDIR = "/tmp/crest_dump";


void print_path(){
  cout << (std::string)SURL << endl;
}


std::string getCrestPath(){
  std::string path = "CREST_SERVER_PATH";
  char * val = getenv( path.c_str() );
  // return val == NULL ? std::string("") : std::string(val);
  if (val != NULL){
    // std::cout << "crestPath = " << val;
    return std::string(val);
  }
  else{
    // throw(runtime_error("Variable " + path  + " not found."));
    std::cerr << "ERROR: environment variable " << path << " not found." << std::endl;
    exit(0);
  }
}

std::string getCrestDir(){
  std::string path = "CREST_FOLDER_PATH";
  char * val = getenv( path.c_str() );
  // return val == NULL ? std::string("") : std::string(val);
  if (val != NULL){
    // std::cout << "crestPath = " << val;
    return std::string(val);
  }
  else{
    // throw(runtime_error("Variable " + path  + " not found."));
    // std::cerr << "ERROR: environment variable " << path << " not found." << std::endl; // old
    return SDIR;
    //exit(0); // old
  }
}

std::string getFileString2(const std::string& path) {
  try{
    std::ifstream ifs(path);
    return std::string((std::istreambuf_iterator<char>(ifs)),
                       (std::istreambuf_iterator<char>()));
  }
  catch(...){
    // std::cerr << "Error" << std::endl;
    throw(runtime_error("Error"));
  }

}

//

bool startsWith(std::string mainStr, std::string toMatch){
  // std::string::find returns 0 if toMatch is found at starting
  // std::cout << "find = " << mainStr.find(toMatch) << std::endl;
  if(mainStr.find(toMatch) == 0){
    return true;
  }
  else{
    return false;
  }
}


bool isFile(std::string str) {
  std::string bstr = "file://";
  return startsWith(str, bstr);
}

std::string getFileName(std::string str){
  std::string st = str;
  // Removing "fil://", i.e. first 7 symbols:
  // str.erase(start_position_to_erase, number_of_symbols);
  st.erase(0, 7);
  return st;
}


bool fileExists(const std::filesystem::path& p, std::filesystem::file_status s = std::filesystem::file_status{}){
  // std::cout << p;
  if(std::filesystem::status_known(s) ? std::filesystem::exists(s) : std::filesystem::exists(p)){
    // std::cout << " exists\n";
    return true;
  }
  else{
    // std::cout << " does not exist\n";
    return false;
  }
}


nlohmann::json getArgToJson(std::string str, CrestClientExt myCrestClient){

  // JSON is in the file i.e.
  // argv[3] contains a string "file://"
  if (isFile(str)){
    // std::cout << "arg is a file" << std::endl;
    std::string filename = getFileName(str);
    // std::cout << "file name = " << filename << std::endl;

    // checking if the file exists
    if (fileExists(filename)){
      std::string strj = getFileString2(filename);

      // file content to JSON conversion
      try{ 
	nlohmann::json js = myCrestClient.getJson(strj);
        /*
        std::cout << "js = " << std::endl
	     	  << js.dump(4) << std::endl;
	*/
        return js;
      }
      catch(...){
	std::cerr << "ERROR: cannot convert file \"" << filename << "\" into JSON format." << std::endl;
        exit(0);
	// std::cout << "M1" << std::endl;
      }
    } // file exists
    else{
      std::cerr << "ERROR: file \"" << filename << "\" does not exit." << std::endl;
      exit(0);
      // std::cout << "M2" << std::endl;
    }  // file does not exists
  }
  else{
    // code to get JSON from argv[3]:
    // std::cout << "arg is not a file" << std::endl;
    //
    if(myCrestClient.isJson(str)){
      // argv[3] is in JSON format:
      nlohmann::json js = myCrestClient.getJson(str);
      /*
      std::cout << "js = " << std::endl
	     	<< js.dump(4) << std::endl;
      */
      return js;
    }
    else{
      // argv[3] cannot be converted to JSON:
      std::cerr << "ERROR: cannot convert \"" << str << "\" to JSON." << std::endl;
      exit(0);
    }
  //
  }
}

//
nlohmann::json fileToJson(std::string filename, CrestClientExt myCrestClient){

  if (fileExists(filename)){
    std::string strj = getFileString2(filename);

    // file content to JSON conversion
    try{ 
      nlohmann::json js = myCrestClient.getJson(strj);
      /*
      std::cout << "js = " << std::endl
	        << js.dump(4) << std::endl;
      */
      return js;
    }
    catch(...){
      std::cerr << "ERROR: cannot convert file \"" << filename << "\" into JSON format." << std::endl;
      exit(0);
    }
  } // file exists
  else {
    std::cerr << "ERROR: cannot open file \"" << filename << "\"." << std::endl;
    exit(0);
  }

}
//

int runTagList(int argc, char * argv[],CrestClientExt myCrestClient)
{
  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   int size = 20;
   int page = 0;

   const char    * short_opt = "hp:s:";
   struct option   long_opt[] =
   {
      {"help",          no_argument,       NULL, 'h'},
      {"page",          required_argument, NULL, 'p'},
      {"size",          required_argument, NULL, 's'},
      {NULL,            0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'p':
	 // printf("p: you entered \"%s\"\n", optarg);

         try {
           page = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong page parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 's':
	 // printf("s: you entered \"%s\"\n", optarg);
         try {
           size = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong size parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 'h':
         printf("Usage: crestCmd get tagList [OPTIONS]\n");
         printf("  -s, --size      page size   (default 20)\n");
         printf("  -p, --page      page number (default 0)\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //
 
   /*
   std::cout << "  size = " << size << std::endl;
   std::cout << "  page = " << page << std::endl;
   */

   try{
     nlohmann::json list2 = myCrestClient.listTagsParams(_page=page,_size=size);
     std::cout << "Tag list :" << " :" << std::endl;
     std::cout << list2.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get the tag list." << std::endl;
     std::cerr << e.what() << std::endl;
   }

   return(0);
}


//

int runCreateTag(int argc, char * argv[],CrestClientExt myCrestClient){
  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;


   /*


   -n --tagname              "name"
   -d --description          "description"
   -t --timetype             "timeType"

   -p --payloadspec          "payloadSpec"

   -s --synchronization      "synchronization"
   -e --endofvalidity        "endOfValidity"
   -l --lastvalidatedtime    "lastValidatedTime"
   -i --insertiontime        "insertionTime"
   -m --modificationtime     "modificationTime"
   -q --in <server|fileSystem> 

   */


   std::string tagName = "";
   std::string desc = "";
   std::string timeType = "";
   std::string file = "";
   std::string sjs = "";
   std::string payloadSpec = "none";
   std::string inParam = "server";

   const char    * short_opt = "hf:j:n:d:t:p:q:";

   struct option   long_opt[] =
   {
      {"help",                 no_argument,       NULL, 'h'},
      {"file",                 required_argument, NULL, 'f'},
      {"json",                 required_argument, NULL, 'j'},
      {"tagName",              required_argument, NULL, 'n'},
      {"description",          required_argument, NULL, 'd'},
      {"timeType",             required_argument, NULL, 't'},
      {"payloadSpec",          required_argument, NULL, 'p'},
      {"in",                   required_argument, NULL, 'q'},
      {NULL,                   0,                 NULL,  0 }
   };


   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         //
         case 'f':
	 // printf("f: you entered \"%s\"\n", optarg);
         try {
           file = optarg;
           // std::cout << "  file = "    << file << std::endl;
	   nlohmann::json js = fileToJson(file,myCrestClient);
           /*
           std::cout << "js = " << std::endl
	             << js.dump(4) << std::endl;
           */
           try{
             myCrestClient.createTag(js);
             std::cout << "Tag was created." << std::endl;
           }
           catch (const std::exception& e){
             std::cerr << "ERROR: cannot create the tag:" << std::endl;
             std::cerr << e.what() << std::endl;
           }
           exit(0);
	 }
         catch (...){
	   std::cout << "ERROR: wrong file name parameter, cannot convert " << optarg << std::endl;
           exit(0);
         }
         break;

         case 'j':
	 // printf("f: you entered \"%s\"\n", optarg);
         try {
           sjs = optarg;
           std::cout << "  json (string) = "    << sjs << std::endl;
           try{ 
             nlohmann::json js = myCrestClient.getJson(sjs);
             /*
             std::cout << "js = " << std::endl
       	        << js.dump(4) << std::endl;
             */

             try{
               myCrestClient.createTag(js);
               std::cout << "Tag was created." << std::endl;
             }
               catch (const std::exception& e){
               std::cerr << "ERROR: cannot create the tag:" << std::endl;
               std::cerr << e.what() << std::endl;
             }
             exit(0);
           }
           catch(...){
             std::cerr << "ERROR: cannot convert string \"" << sjs << "\" into JSON format." << std::endl;
             exit(0);
           }
           exit(0);
	 }
         catch (...){
	   std::cout << "ERROR: wrong JSON string: " << optarg << std::endl;
           exit(0);
         }
         break;

         //


         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         try {
           tagName = optarg;
	 }
         catch (...){
	   std::cout << "ERROR: wrong page parameter, cannot convert " << optarg << std::endl;
           exit(0);
         }
         break;


         case 'd':
	 // printf("d: you entered \"%s\"\n", optarg);
         desc = optarg;
         break;

         case 't':
	 // printf("t: you entered \"%s\"\n", optarg);
         timeType = optarg;
         break;

         case 'p':
	 // printf("p: you entered \"%s\"\n", optarg);
         payloadSpec = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd create tag [OPTIONS]\n");
         printf("  -f, --file             file with tag JSON\n");
         printf("  -j, --json             JSON string\n");
         printf("  -n, --tagName          tag name\n");
         printf("  -d, --description      tag description\n");
         printf("  -t, --timeType         time type\n");
         printf("  -p, --payloadSpec      payload specification\n");
         printf("  -h, --help             print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
   if (optind < argc){
     printf ("ERROR: non-option ARGV-elements: ");
       while (optind < argc) {
       printf ("%s ", argv[optind++]);
     }
     putchar ('\n');
     exit(0);
   }
   //

   /*
   std::cout << "  tagName = "       << tagName     << std::endl;
   std::cout << "  description = "   << desc        << std::endl;
   std::cout << "  timeType = "      << timeType    << std::endl;
   std::cout << "  payloadSpec = "   << payloadSpec << std::endl;
   */

   // Required Parameter Checking:
   if (tagName == ""){
     std::cout << "ERROR: please enter the tag name!" << std::endl;
     exit(0);
   }
   else if (desc == ""){
     std::cout << "ERROR: please enter the tag description!" << std::endl;
     exit(0);
   }
   else if (timeType == ""){
     std::cout << "ERROR: please enter the timeType!" << std::endl;
     exit(0);
   }
   else{
     nlohmann::json js = 
     {
       {"description", desc},
       {"endOfValidity", 0},
       {"lastValidatedTime", 0},
       {"synchronization", "none"},
       {"payloadSpec", payloadSpec},
       {"name", tagName}, 
       {"timeType", timeType}
     };
     try {
       myCrestClient.createTag(js);
       std::cout << "Tag was created." << std::endl;
     }
     catch(...){
       std::cerr << "ERROR: cannot create the tag" << std::endl; 
    }
   }

   return(0);
}

//

int runFindAllIovs(int argc, char * argv[],CrestClientExt myCrestClient)
{
  
  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   int size = 20;
   int page = 0;
   std::string tagName = "";
   std::string sort ="id.since:ASC";
   std::string dateFormat = "ms";

   const char    * short_opt = "hn:p:s:o:d:";
   struct option   long_opt[] =
   {
      {"help",                no_argument,       NULL, 'h'},
      {"tagName",             required_argument, NULL, 'n'},
      {"page",                required_argument, NULL, 'p'},
      {"size",                required_argument, NULL, 's'},
      {"sort",                required_argument, NULL, 'o'},
      {"dateFormat",          required_argument, NULL, 'd'},
      {NULL,            0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagName = optarg;
         break;

         case 'p':
	 // printf("p: you entered \"%s\"\n", optarg);
         try {
           page = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong page parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 's':
	// printf("s: you entered \"%s\"\n", optarg);
         try {
           size = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong size parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 'o':
	 // printf("o: you entered \"%s\"\n", optarg);
         sort = optarg;
         break;

         case 'd':
	 // printf("p: you entered \"%s\"\n", optarg);
         dateFormat = optarg;
         break;


         case 'h':
         printf("Usage: crestCmd get iovList [OPTIONS]\n");
         printf("  -n, --tagname   page size   (required parameter)\n");
         printf("  -s, --size      page size   (default 20)\n");
         printf("  -p, --page      page number (default 0)\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
   if (optind < argc){
     printf ("ERROR: non-option ARGV-elements: ");
     while (optind < argc) {
       printf ("%s ", argv[optind++]);
     }
     putchar ('\n');
     exit(0);
   }
   //

   /*
   std::cout << "  tagName = "       << tagName << std::endl;
   std::cout << "  size = "          << size << std::endl;
   std::cout << "  page = "          << page << std::endl;
   std::cout << "  sort = "          << sort << std::endl;
   std::cout << "  dateFormat = "    << dateFormat << std::endl;
   */

   // Required Parameter Checking:
   if (tagName == ""){
     std::cout << "ERROR: please enter the tag name!" << std::endl;
     exit(0);
   }

   try{
     nlohmann::json list2 = myCrestClient.findAllIovsParams(tagName,_page=page,_size=size,_sort=sort,_dateformat=dateFormat);
     std::cout << "IOV list =" << std::endl;
     std::cout << list2.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get the iov list." << std::endl;
     std::cerr << e.what() << std::endl;
   }
   //

   return(0);
}

//
int runGetBlob(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string hash = "";

   const char    * short_opt = "hw:";
   struct option   long_opt[] =
   {
      {"help",          no_argument,       NULL, 'h'},
      {"hash",          required_argument, NULL, 'w'},
      {NULL,            0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'w':
	 // printf("w: you entered \"%s\"\n", optarg);
         hash = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd get blob [OPTIONS]\n");
         printf("  -w, --hash      hash\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  hash = " << hash << std::endl;
   if (hash == ""){
     std::cerr << "ERROR: please enter hash code! Example:" << std::endl;
     std::cerr << "  crestCmd get blob --hash 9834876dcfb05cb167a5c24953eba58c4ac89b1adf57f28f2f9d09af107ee8f0" << std::endl;
     exit(0);
   }

   try{
     string blob = myCrestClient.getBlob(hash);
     std::cout << "Blob (" << hash << ") ="  << std::endl;
     std::cout << blob << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get a blob for the hash " << hash << "." << std::endl;
   }

   return(0);
}

//
int runGetTag(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string tagname = "";
   std::string fromParam = "server"; // server or fileSystem

   const char    * short_opt = "hn:q:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tagName",          required_argument, NULL, 'n'},
      {"from",             required_argument, NULL, 'q'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagname = optarg;
         break;

         case 'q':
	 // printf("q: you entered \"%s\"\n", optarg);
         fromParam = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd get tag [OPTIONS]\n");
         printf("  -n, --tagName   tag name\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  tagName = " << tagname << std::endl;
   if (tagname == ""){
     std::cerr << "ERROR: please enter tag name! Example:" << std::endl;
     std::cerr << "  crestCmd get tag --tagName test_MvG3b" << std::endl;
     exit(0);
   }

   /*
   if (!((fromParam != "server") || (fromParam != "fileStorage"))){
     std::cerr << "ERROR: wrong --from (-q) parameter. It has to be server or fileStorage." << std::endl;
     exit(0);
   }
   else {
     std::cout << "--from = " << fromParam << std::endl;
   }
   */

   if (fromParam == "server") {
     std::cout << "--from = " << fromParam << std::endl;

     try{
       nlohmann::json tag_info = myCrestClient.findTag(tagname);
       std::cout << std::endl << "tag(" << tagname << ")=" << std::endl 
                 << tag_info.dump(4) << std::endl; 
     }
     catch(const std::exception& e){
       std::cerr << "ERROR: cannot get a tag for the name " << tagname << "." << std::endl;
     }
     exit(0);
   }
   else if (fromParam == "fileStorage") {
     std::cout << "--from = " << fromParam << std::endl;
     std::string dir = getCrestDir();
     std::cout << "Crest Server directory = " << dir << std::endl;
     CrestClientExt fsCrestClient = CrestClientExt(true,dir);

     try{
       nlohmann::json tag_info = fsCrestClient.findTag(tagname);
       std::cout << std::endl << "tag(" << tagname << ")=" << std::endl 
                 << tag_info.dump(4) << std::endl; 
     }
     catch(const std::exception& e){
       std::cerr << "ERROR: cannot get a tag for the name " << tagname << "." << std::endl;
     }

     exit(0);
   }
   else {
     std::cerr << "ERROR: wrong --from (-q) parameter - " << fromParam << ". It has to be server or fileStorage." << std::endl;
     exit(0);
   }

   /*
   try{
     nlohmann::json tag_info = myCrestClient.findTag(tagname);
     std::cout << std::endl << "tag(" << tagname << ")=" << std::endl 
               << tag_info.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get a tag for the name " << tagname << "." << std::endl;
   }
   */

   return(0);
}

//
int runIovAndPayload(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   /*
   //CREATE iovAndPayload

   -e --endTime   (uint64_t)
   -p --payload   (string)
   -s --since     (int)
   -n --tagName   (string)
   -h --help
   */


   int c;

   std::string tagName = "";
   std::string payload = "";
   int since = -1;
   int endTime = -1;

   const char    * short_opt = "hn:p:e:s:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tagName",          required_argument, NULL, 'n'},
      {"payload",          required_argument, NULL, 'p'},
      {"endtime",          required_argument, NULL, 'e'},
      {"since",            required_argument, NULL, 's'},
      {NULL,               0,                 NULL,  0 }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("t: you entered \"%s\"\n", optarg);
         tagName = optarg;
         break;

         case 'p':
	 // printf("p: you entered \"%s\"\n", optarg);
         payload = optarg;
         break;

         case 'e':
	 // printf("s: you entered \"%s\"\n", optarg);
         try {
           endTime = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong size parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 's':
	 // printf("s: you entered \"%s\"\n", optarg);
         try {
           since = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong size parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;


         case 'h':
         printf("Usage: crestCmd create iovAndPayload [OPTIONS]\n");
         printf("  -n, --tagName    tag name\n");
         printf("  -p, --payload    payload\n");
         printf("  -e, --endTime    end time\n");
         printf("  -s, --since      since\n");
         printf("  -h, --help       print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   /*
   std::cout << "  tagName = " << tagName << std::endl;
   std::cout << "  payload = " << payload << std::endl;
   std::cout << "  endTime = " << endTime << std::endl;
   std::cout << "  since = "   << since   << std::endl;
   */

   if (tagName == ""){
     std::cerr << "ERROR: please enter tag name!" << std::endl;
     exit(0);
   }
   if (payload == ""){
     std::cerr << "ERROR: please enter payload!" << std::endl;
     exit(0);
   }
   if (endTime == -1){
     std::cerr << "ERROR: please enter endTime!" << std::endl;
     exit(0);
   }
   if (since == -1){
     std::cerr << "ERROR: please enter since!" << std::endl;
     exit(0);
   }

   uint64_t endt = endTime;
   nlohmann::json js0 = {
     {"payloadHash",payload},
     {"since",since}
   };

   nlohmann::json js;
   js[0] = js0;

   // std::cout << "js =" << std::endl;
   // std::cout << js.dump(4) << std::endl;

   try{
     myCrestClient.storeBatchPayloads(tagName, endt, js);
   }
   catch (const std::exception& e){
     std::cerr << "ERROR: cannot create an iov and a payload." << std::endl;
     std::cerr << e.what() << std::endl;
   }

   return(0);
}
//

int runGetPayloadMetaInfo(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string hash = "";

   const char    * short_opt = "hw:";
   struct option   long_opt[] =
   {
      {"help",          no_argument,       NULL, 'h'},
      {"hash",          required_argument, NULL, 'w'},
      {NULL,            0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'w':
	 // printf("w: you entered \"%s\"\n", optarg);
         hash = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd get payloadMetaInfo [OPTIONS]\n");
         printf("  -w, --hash      hash\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  hash = " << hash << std::endl;
   if (hash == ""){
     std::cerr << "ERROR: please enter hash code! Example:" << std::endl;
     std::cerr << "  crestCmd get blob --hash 9834876dcfb05cb167a5c24953eba58c4ac89b1adf57f28f2f9d09af107ee8f0" << std::endl;
     exit(0);
   }

   try{
     nlohmann::json info = myCrestClient.getPayloadMetaInfo(hash);
     std::cout << "payloadMetaInfo ( " << hash << " ) = " << std::endl;
     std::cout << info.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get a payload meta info for the hash " << hash << "." << std::endl;
   }

   return(0);
}

//

int runGetPayloadTagInfo(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string tagname = "";

   const char    * short_opt = "hn:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tagName",          required_argument, NULL, 'n'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagname = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd get payloadTagInfo [OPTIONS]\n");
         printf("  -n, --tagName   tag name (optional)\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  tagName = " << tagname << std::endl;
   if (tagname == ""){
     try{
       nlohmann::json info = myCrestClient.listPayloadTagInfo();
       std::cout << std::endl << "payloadTagInfo =" << std::endl 
                 << info.dump(4) << std::endl;
     }
     catch(const std::exception& e){
       std::cerr << "ERROR: cannot get a tag for the name " << tagname << "." << std::endl;
     }
   }
   else {
     try{
       nlohmann::json info = myCrestClient.listPayloadTagInfo(tagname);
       std::cout << std::endl << "payloadTagInfo(" << tagname << ")=" << std::endl
                 << info.dump(4) << std::endl;
     }
     catch(const std::exception& e){
       std::cerr << "ERROR: cannot get a tag for the name " << tagname << "." << std::endl;
     }

   }

   return(0);
}

//

int runGetGlobalTag(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string tagname = "";

   const char    * short_opt = "hn:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tagName",          required_argument, NULL, 'n'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagname = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd get globalTag [OPTIONS]\n");
         printf("  -n, --tagName   tag name\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  tagName = " << tagname << std::endl;
   if (tagname == ""){
     std::cerr << "ERROR: please enter tag name! Example:" << std::endl;
     std::cerr << "  crestCmd get tag --tagName test_MvG3b" << std::endl;
     exit(0);
   }

   try{
     nlohmann::json tag_info = myCrestClient.findGlobalTag(tagname);
     std::cout << std::endl << "globalTag(" << tagname << ")=" << std::endl 
               << tag_info.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get a tag for the name " << tagname << "." << std::endl;
     std::cerr << e.what() << std::endl;
   }

   return(0);
}

//


int runGetGlobalTagList(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;

   const char    * short_opt = "h";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'h':
         printf("Usage: crestCmd get globalTagList [OPTIONS]\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //


   try{
     nlohmann::json info = myCrestClient.listGlobalTags();
     std::cout << std::endl << "tagList =" << std::endl 
               << info.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get a tag list." << std::endl;
   }

   return(0);
}

//

int runRemoveTag(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string tagname = "";

   const char    * short_opt = "hn:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tagName",          required_argument, NULL, 'n'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagname = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd remove tag [OPTIONS]\n");
         printf("  -n, --tagName   tag name\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  tagName = " << tagname << std::endl;
   if (tagname == ""){
     std::cerr << "ERROR: please enter tag name! Example:" << std::endl;
     std::cerr << "  crestCmd remove tag --tagName test_MvG3b" << std::endl;
     exit(0);
   }

   try{
     myCrestClient.removeTag(tagname);
     std::cout << "Tag with name " << tagname << " removed." << std::endl; 
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot remove the tag with the name " << tagname << "." << std::endl;
     std::cerr << e.what() << tagname << "." << std::endl;
   }

   return(0);
}


//

int runRemoveGlobalTag(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string tagname = "";

   const char    * short_opt = "hn:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tagName",          required_argument, NULL, 'n'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagname = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd remove globalTag [OPTIONS]\n");
         printf("  -n, --tagName   tag name\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  tagName = " << tagname << std::endl;
   if (tagname == ""){
     std::cerr << "ERROR: please enter tag name! Example:" << std::endl;
     std::cerr << "  crestCmd remove globalTag --tagName test_MvG3b" << std::endl;
     exit(0);
   }

   try{
     myCrestClient.removeGlobalTag(tagname);
     std::cout << "Global tag with name " << tagname << " removed." << std::endl; 
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot remove the global tag with the name " << tagname << "." << std::endl;
     std::cerr << e.what() << std::endl;
   }

   return(0);
}

//

int runCreateGlobalTag(int argc, char * argv[],CrestClientExt myCrestClient)
{
  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;


/*

Global Tag (parameters):

-n  --tagName
-d  --description
-r  --release
-s  --scenario
-t  --type
-w  --workflow
-v  --validity

-i  --insertionTime
-l  --snapshotTime

*/


   std::string tagName = "";
   std::string description = "";
   std::string type = "";
   std::string release = "";
   std::string scenario = "";
   std::string workflow = "";
   std::string validity = "";

   std::string insertionTime = "";
   std::string snapshotTime = "";

   std::string file = "";
   std::string sjs = "";

   std::string payloadSpec = "none";

   const char    * short_opt = "hf:j:n:d:t:r:w:s:v:i:l:";

   struct option   long_opt[] =
   {
      {"help",                 no_argument,       NULL, 'h'},
      {"file",                 required_argument, NULL, 'f'},
      {"json",                 required_argument, NULL, 'j'},
      {"tagName",              required_argument, NULL, 'n'},
      {"description",          required_argument, NULL, 'd'},
      {"type",                 required_argument, NULL, 't'},
      {"release",              required_argument, NULL, 'r'},
      {"workflow",             required_argument, NULL, 'w'},
      {"scenario",             required_argument, NULL, 's'},
      {"validity",             required_argument, NULL, 'v'},

      {"insertionTime",        required_argument, NULL, 'i'},
      {"snapshotTime",         required_argument, NULL, 'l'},
      {NULL,                   0,                 NULL,  0 }
   };


   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         //
         case 'f':
	 // printf("f: you entered \"%s\"\n", optarg);
         try {
           file = optarg;
           // std::cout << "  file = "    << file << std::endl;
	   nlohmann::json js = fileToJson(file,myCrestClient);
           /*
           std::cout << "js = " << std::endl
	             << js.dump(4) << std::endl;
           */
           try{
             myCrestClient.createGlobalTag(js);
             std::cout << "Tag was created." << std::endl;
           }
           catch (const std::exception& e){
             std::cerr << "ERROR: cannot create the tag:" << std::endl;
             std::cerr << e.what() << std::endl;
           }
           exit(0);
	 }
         catch (...){
	   std::cout << "ERROR: wrong file name parameter, cannot convert " << optarg << std::endl;
           exit(0);
         }
         break;

         case 'j':
	 // printf("f: you entered \"%s\"\n", optarg);
         try {
           sjs = optarg;
           std::cout << "  json (string) = "    << sjs << std::endl;
           try{ 
             nlohmann::json js = myCrestClient.getJson(sjs);
             /*
             std::cout << "js = " << std::endl
       	        << js.dump(4) << std::endl;
             */

             try{
               myCrestClient.createGlobalTag(js);
               std::cout << "Tag was created." << std::endl;
             }
               catch (const std::exception& e){
               std::cerr << "ERROR: cannot create the tag:" << std::endl;
               std::cerr << e.what() << std::endl;
             }
             exit(0);
           }
           catch(...){
             std::cerr << "ERROR: cannot convert string \"" << sjs << "\" into JSON format." << std::endl;
             exit(0);
           }
           exit(0);
	 }
         catch (...){
	   std::cout << "ERROR: wrong JSON string: " << optarg << std::endl;
           exit(0);
         }


         break;
         case 'n':
         tagName = optarg;
         break;

         case 'd':
	 // printf("d: you entered \"%s\"\n", optarg);
         description = optarg;
         break;

         case 't':
	 // printf("t: you entered \"%s\"\n", optarg);
         type = optarg;
         break;

         case 'r':
	 // printf("p: you entered \"%s\"\n", optarg);
         release = optarg;
         break;

         case 's':
	 // printf("p: you entered \"%s\"\n", optarg);
         scenario = optarg;
         break;

         case 'w':
	 // printf("p: you entered \"%s\"\n", optarg);
         workflow = optarg;
         break;

         case 'v':
	 // printf("p: you entered \"%s\"\n", optarg);
         validity = optarg;
         break;

         case 'i':
	 // printf("p: you entered \"%s\"\n", optarg);
         insertionTime = optarg;
         break;

         case 'l':
	 // printf("p: you entered \"%s\"\n", optarg);
         snapshotTime = optarg;
         break;


         case 'h':
         printf("Usage: crestCmd create globalTag [OPTIONS]\n");
         printf("  -f, --file             file with global tag JSON\n");
         printf("  -j, --json             JSON string\n");
         printf("  -n, --tagName          global tag name\n");
         printf("  -d, --description      global tag description\n");
         printf("  -t, --type             type\n");
         printf("  -w, --workflow         workflow\n");
         printf("  -v, --validity         validity\n");
         printf("  -s, --scenario         scenario\n");
         printf("  -r, --release          release\n");
         printf("  -i, --insertionTime    insetrion time\n");
         printf("  -l, --snapshotTime     snapshot time\n");
         printf("  -h, --help             print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
   if (optind < argc){
     printf ("ERROR: non-option ARGV-elements: ");
       while (optind < argc) {
       printf ("%s ", argv[optind++]);
     }
     putchar ('\n');
     exit(0);
   }
   //

/*

Global Tag (parameters):

-n  --tagName
-d  --description
-r  --release
-s  --scenario
-t  --type
-w  --workflow
-v  --validity

-i  --insertionTime
-l  --snapshotTime

*/

   //
   std::cout << "  tagName = "       << tagName     << std::endl;
   std::cout << "  description = "   << description        << std::endl;
   std::cout << "  type = "          << type    << std::endl;
   std::cout << "  release = "       << release << std::endl;
   std::cout << "  scenario = "      << scenario     << std::endl;
   std::cout << "  workflow = "      << workflow        << std::endl;
   std::cout << "  validity = "      << validity    << std::endl;

   std::cout << "  insertionTime = " << insertionTime << std::endl;
   std::cout << "  snapshotTime = "  << snapshotTime << std::endl;
   //

   nlohmann::json js; 

   if (tagName != ""){
     js["name"] = tagName;
   }
   if (description != ""){
     js["description"] = description;
   }
   if (validity != ""){
     js["validity"] = validity;
   }
   if (release != ""){
     js["release"] = release;
   }
   if (scenario != ""){
     js["scenario"] = scenario;
   }
   if (type != ""){
     js["type"] = type;
   }
   if (workflow != ""){
     js["workflow"] = workflow;
   }
   if (snapshotTime != ""){
     js["snapshotTime"] = snapshotTime;
   }
   if (insertionTime != ""){
     js["insertionTime"] = insertionTime;
   }

   //
   std::cout << "js =" << std::endl;
   std::cout << js.dump(4) << std::endl;
   //


   try {
     myCrestClient.createGlobalTag(js);
     std::cout << "Global tag was created." << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot create the global tag" << std::endl; 
     std::cerr << e.what() << std::endl;
   }

   return(0);
}

//

int runGTList(int argc, char * argv[],CrestClientExt myCrestClient,const std::string& method){
  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string name = "";
   int size = 20;
   int page = 0;
   std::string sort = "name:ASC";

   const char    * short_opt = "hn:p:s:o:";
   struct option   long_opt[] =
   {
      {"help",          no_argument,       NULL, 'h'},
      {"name",          required_argument, NULL, 'n'},
      {"page",          required_argument, NULL, 'p'},
      {"size",          required_argument, NULL, 's'},
      {"sort",          required_argument, NULL, 'o'},
      {NULL,            0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         name = optarg;
         break;

         case 'p':
	 // printf("p: you entered \"%s\"\n", optarg);

         try {
           page = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong page parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 's':
	 // printf("s: you entered \"%s\"\n", optarg);
         try {
           size = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong size parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 'o':
	 // printf("n: you entered \"%s\"\n", optarg);
         sort = optarg;
         break;

         case 'h':
	 std::cout << "Usage: crestCmd get " << method << " [OPTIONS]" << std::endl;
         printf("  -n, --name      name pattern\n");
         printf("  -s, --size      page size   (default 20)\n");
         printf("  -p, --page      page number (default 0)\n");
         printf("  -o, --sort      sorting order\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //
 
   /*
   std::cout << "  name = " << name << std::endl;
   std::cout << "  size = " << size << std::endl;
   std::cout << "  page = " << page << std::endl;
   std::cout << "  sort = " << sort << std::endl;
   */

   try{
     if (method == "tagList"){
       nlohmann::json list = myCrestClient.listTagsParams(_name=name,_page=page,_size=size,_sort=sort);
       std::cout << "Tag list :" << " :" << std::endl;
       std::cout << list.dump(4) << std::endl;
     }
     else if (method == "globalTagList"){
       nlohmann::json list = myCrestClient.listGlobalTagsParams(_name=name,_page=page,_size=size,_sort=sort);
       std::cout << "Global tag list :" << " :" << std::endl;
       std::cout << list.dump(4) << std::endl;
     }
     else{
       std::cout << "ERROR: (runGTList) wrong method." << std::endl;
       exit(0);
     }
   }
   catch(const std::exception& e){
     if (method == "tagList"){
       std::cerr << "ERROR: cannot get the tag list." << std::endl;
     }
     else if (method == "globalTagList"){
       std::cerr << "ERROR: cannot get the global tag list." << std::endl;
     }
     else{
       std::cout << "ERROR: (runGTList) wrong method." << std::endl;
       exit(0);
     }
     std::cerr << e.what() << std::endl;
   }

   return(0);
}

void printMethods(){

  printf("Command List:\n");

  printf("\n1) Get list methods:\n");

  printf("  crestCmd get tagList [OPTIONS]\n");
  printf("  crestCmd get globalTagList [OPTIONS]\n");
  printf("  crestCmd get globalTagMap [OPTIONS]\n");
  printf("  crestCmd get iovList [OPTIONS]\n");

  printf("\n2) Get methods:\n");

  printf("  crestCmd get tag [OPTIONS]\n");
  printf("  crestCmd get tagMetaInfo [OPTIONS]\n");

  printf("  crestCmd get globalTag [OPTIONS]\n");
  printf("  crestCmd get blob [OPTIONS]\n");
  printf("  crestCmd get payloadMetaInfo [OPTIONS]\n");
  printf("  crestCmd get payloadTagInfo [OPTIONS]\n");

  printf("\n3) Create methods:\n");

  printf("  crestCmd create tag [OPTIONS]\n");
  printf("  crestCmd create tagMetaInfo [OPTIONS]\n");
  printf("  crestCmd create globalTag [OPTIONS]\n");
  printf("  crestCmd create globalTagMap [OPTIONS]\n");
  printf("  crestCmd create iovAndPayload [OPTIONS]\n");

  printf("\n4) Remove methods:\n");

  printf("  crestCmd remove tag [OPTIONS]\n");
  printf("  crestCmd remove globalTag [OPTIONS]\n");

  printf("\n");

  return;
}

//

int runCreateGlobalTagMap(int argc, char * argv[],CrestClientExt myCrestClient) {
  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;


   /*

   -g --globalTag            "globalTagName"
   -n --tagname              "tagName"
   -r --record               "record"
   -l --label                "label"

   */


   std::string tagName = "";
   std::string globalTagName = "";
   std::string record = "";
   std::string label = "";
   std::string file = "";
   std::string sjs = "";

   const char    * short_opt = "hf:j:g:n:r:l:";

   struct option   long_opt[] =
   {
      {"help",                 no_argument,       NULL, 'h'},
      {"file",                 required_argument, NULL, 'f'},
      {"json",                 required_argument, NULL, 'j'},
      {"globalTagName",        required_argument, NULL, 'g'},
      {"tagName",              required_argument, NULL, 'n'},
      {"record",               required_argument, NULL, 'r'},
      {"label",                required_argument, NULL, 'l'},
      {NULL,                   0,                 NULL,  0 }
   };


   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         //
         case 'f':
	 // printf("f: you entered \"%s\"\n", optarg);
         try {
           file = optarg;
           // std::cout << "  file = "    << file << std::endl;
	   nlohmann::json js = fileToJson(file,myCrestClient);
           /*
           std::cout << "js = " << std::endl
	             << js.dump(4) << std::endl;
           */
           try{

             myCrestClient.createGlobalTagMap(js);
             std::cout << "Global tag map was created." << std::endl;
           }
           catch (const std::exception& e){
             std::cerr << "ERROR: cannot create the global tag map:" << std::endl;
             std::cerr << e.what() << std::endl;
           }
           exit(0);
	 }
         catch (...){
	   std::cout << "ERROR: wrong file name parameter, cannot convert " << optarg << std::endl;
           exit(0);
         }
         break;

         case 'j':
	 // printf("f: you entered \"%s\"\n", optarg);
         try {
           sjs = optarg;
           std::cout << "  json (string) = "    << sjs << std::endl;
           try{ 
             nlohmann::json js = myCrestClient.getJson(sjs);
             /*
             std::cout << "js = " << std::endl
       	        << js.dump(4) << std::endl;
             */

             try{

               myCrestClient.createGlobalTagMap(js);
               std::cout << "Global tag map was created." << std::endl;
             }
               catch (const std::exception& e){
               std::cerr << "ERROR: cannot create the global tag map:" << std::endl;
               std::cerr << e.what() << std::endl;
             }
             exit(0);
           }
           catch(...){
             std::cerr << "ERROR: cannot convert string \"" << sjs << "\" into JSON format." << std::endl;
             exit(0);
           }
           exit(0);
	 }
         catch (...){
	   std::cout << "ERROR: wrong JSON string: " << optarg << std::endl;
           exit(0);
         }
         break;

         //


         case 'g':
	 // printf("g: you entered \"%s\"\n", optarg);
         globalTagName = optarg;
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagName = optarg;
         break;

         case 'r':
	 // printf("r: you entered \"%s\"\n", optarg);
         record = optarg;
         break;

         case 'l':
	 // printf("l: you entered \"%s\"\n", optarg);
         label = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd create globalTagMap [OPTIONS]\n");
         printf("  -f, --file             file with tag JSON\n");
         printf("  -j, --json             JSON string\n");
         printf("  -g, --globalTagName    global tag name\n");
         printf("  -n, --tagName          tag name\n");
         printf("  -r, --record           record\n");
         printf("  -l, --label            label\n");
         printf("  -h, --help             print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
   if (optind < argc){
     printf ("ERROR: non-option ARGV-elements: ");
       while (optind < argc) {
       printf ("%s ", argv[optind++]);
     }
     putchar ('\n');
     exit(0);
   }
   //

   /*
   std::cout << "  globalTagName = " << globalTagName   << std::endl;
   std::cout << "  tagName = "       << tagName         << std::endl;
   std::cout << "  record = "        << record          << std::endl;
   std::cout << "  label = "         << label           << std::endl;
   */

   // Required Parameter Checking:
   if (globalTagName == ""){
     std::cout << "ERROR: please enter the global tag name!" << std::endl;
     exit(0);
   }
   if (tagName == ""){
     std::cout << "ERROR: please enter the tag name!" << std::endl;
     exit(0);
   }
   else if (record == ""){
     std::cout << "ERROR: please enter the record!" << std::endl;
     exit(0);
   }
   else if (label == ""){
     std::cout << "ERROR: please enter the label!" << std::endl;
     exit(0);
   }
   else{
     nlohmann::json js = 
     {
       {"globalTagName", globalTagName},
       {"tagName", tagName},
       {"record", record},
       {"label", "label"}
     };
     try {
       myCrestClient.createGlobalTagMap(js);
       std::cout << "Global tag map was created." << std::endl;
     }
     catch(...){
       std::cerr << "ERROR: cannot create the tag" << std::endl; 
    }
   }

   return(0);
}

//

int runGetGlobalTagMap(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string tagname = "";

   const char    * short_opt = "hg:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"globalTagName",    required_argument, NULL, 'g'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'g':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagname = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd get globalTagMap [OPTIONS]\n");
         printf("  -g, --globalTagName   tag name (required parameter)\n");
         printf("  -h, --help            print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  globalTagName = " << tagname << std::endl;
   if (tagname == ""){
     std::cerr << "ERROR: please enter a global tag name! Example:" << std::endl;
     std::cerr << "  crestCmd get globalTagMap --globalTagName test_MvG3b" << std::endl;
     exit(0);
   }

   try{
     nlohmann::json tag_map = myCrestClient.findGlobalTagMap(tagname);
     std::cout << std::endl << "global tag map(" << tagname << ")=" << std::endl 
               << tag_map.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get a tag for the name " << tagname << "." << std::endl;
   }

   return(0);
}

//

bool directory_exists(char *dname){
 DIR *di=opendir(dname); //open the directory
 if(di) return true; //can open=>return true
 else return false;//otherwise return false
 closedir(di);
}

//

int runDumpTag(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string tagname = "";

   const char    * short_opt = "hn:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tagName",          required_argument, NULL, 'n'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagname = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd dump tag [OPTIONS]\n");
         printf("  -n, --tagName   tag name\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  tagName = " << tagname << std::endl;
   if (tagname == ""){
     std::cerr << "ERROR: please enter tag name! Example:" << std::endl;
     std::cerr << "  crestCmd get tag --tagName test_MvG3b" << std::endl;
     exit(0);
   }

   nlohmann::json tag_info = myCrestClient.findTag(tagname);
   try{
     // nlohmann::json tag_info = myCrestClient.findTag(tagname);
     std::cout << std::endl << "tag(" << tagname << ")=" << std::endl 
               << tag_info.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get a tag for the name " << tagname << "." << std::endl;
   }

   // block to write the data in a file

   CrestClientExt myCrestClientFS = CrestClientExt(true);

   try{

     // OLD VERSION:
     /*
     nlohmann::json tag_info = myCrestClient.findTag(tagname);
     std::cout << std::endl << "tag(" << tagname << ")=" << std::endl 
               << tag_info.dump(4) << std::endl;
     */

     std::string defaultDir =  "/tmp/crest_dump";
     // const char *cstr = defaultDir.c_str();
     if (directory_exists(strdup(defaultDir.c_str()))){
       std::cout << "Directory exists." << std::endl;
     }
     else  {
       std::cerr << "ERROR: directory does not exists." << std::endl;
       exit(0);
     }
     myCrestClientFS.createTag(tag_info[0]);
     std::cout << "Tag was saved on file system." << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot save a tag on file system." << std::endl;
     std::cerr << e.what() << std::endl;
   }
   // block to write the data in a file (end)

   return(0);
}

//

int runGetTagMetaInfo(int argc, char * argv[],CrestClientExt myCrestClient){

  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;
   std::string tagname = "";

   const char    * short_opt = "hn:";
   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tagName",          required_argument, NULL, 'n'},
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         tagname = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd get tagMetaInfo [OPTIONS]\n");
         printf("  -n, --tagName   tag name\n");
         printf("  -h, --help      print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  tagName = " << tagname << std::endl;
   if (tagname == ""){
     std::cerr << "ERROR: please enter tag name! Example:" << std::endl;
     std::cerr << "  crestCmd get tagMetaInfo --tagName test_MvG3b" << std::endl;
     exit(0);
   }

   try{
     nlohmann::json tag_info = myCrestClient.getTagMetaInfo(tagname);
     std::cout << std::endl << "tagMetaInfo(" << tagname << ")=" << std::endl 
               << tag_info.dump(4) << std::endl;
   }
   catch(const std::exception& e){
     std::cerr << "ERROR: cannot get a tag meta info for the tag " << tagname << "." << std::endl;
   }

   return(0);
}

//

int runCreateTagMetaInfo(int argc, char * argv[],CrestClientExt myCrestClient)
{
  /*
   for (int i = 0; i < argc; i++){
     std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
   }
  */

   int c;



/*

-n  --tagName
-d  --description
-i  --insertionTime
-t  --tagInfo
-c  --chansize
-l  --colsize

-f  --file
-j  --json

*/


   std::string tagName = "";
   std::string desc = "";
   std::string tagInfo ="";
   std::string insertionTime = "";
   std::string file = "";
   std::string sjs = "";

   int chansize = 0;
   int colsize = 0;


   const char    * short_opt = "hf:j:n:d:t:c:l:i:";

   struct option   long_opt[] =
   {
      {"help",                 no_argument,       NULL, 'h'},
      {"file",                 required_argument, NULL, 'f'},
      {"json",                 required_argument, NULL, 'j'},
      {"tagName",              required_argument, NULL, 'n'},
      {"description",          required_argument, NULL, 'd'},
      {"tagInfo",              required_argument, NULL, 't'},
      {"chansize",             required_argument, NULL, 'c'},
      {"colsize",              required_argument, NULL, 'l'},
      {"insertionTime",        required_argument, NULL, 'i'},
      {NULL,                   0,                 NULL,  0 }
   };


   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         //
         case 'f':
	 // printf("f: you entered \"%s\"\n", optarg);
         try {
           file = optarg;
           // std::cout << "  file = "    << file << std::endl;
	   nlohmann::json js = fileToJson(file,myCrestClient);
           /*
           std::cout << "js = " << std::endl
	             << js.dump(4) << std::endl;
           */
           try{
             myCrestClient.createTagMetaInfo(js);
             std::cout << "Tag meta info was created." << std::endl;
           }
           catch (const std::exception& e){
             std::cerr << "ERROR: cannot create the tag meta info:" << std::endl;
             std::cerr << e.what() << std::endl;
           }
           exit(0);
	 }
         catch (...){
	   std::cout << "ERROR: wrong file name parameter, cannot convert " << optarg << std::endl;
           exit(0);
         }
         break;

         case 'j':
	 // printf("f: you entered \"%s\"\n", optarg);
         try {
           sjs = optarg;
           std::cout << "  json (string) = "    << sjs << std::endl;
           try{ 
             nlohmann::json js = myCrestClient.getJson(sjs);
             /*
             std::cout << "js = " << std::endl
       	        << js.dump(4) << std::endl;
             */

             try{
               myCrestClient.createTagMetaInfo(js);
               std::cout << "Tag meta info was created." << std::endl;
             }
               catch (const std::exception& e){
               std::cerr << "ERROR: cannot create the tag meta info:" << std::endl;
               std::cerr << e.what() << std::endl;
             }
             exit(0);
           }
           catch(...){
             std::cerr << "ERROR: cannot convert string \"" << sjs << "\" into JSON format." << std::endl;
             exit(0);
           }
           exit(0);
	 }
         catch (...){
	   std::cout << "ERROR: wrong JSON string: " << optarg << std::endl;
           exit(0);
         }
         break;

         //


         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         try {
           tagName = optarg;
	 }
         catch (...){
	   std::cout << "ERROR: wrong page parameter, cannot convert " << optarg << std::endl;
           exit(0);
         }
         break;


         case 'd':
	 // printf("d: you entered \"%s\"\n", optarg);
         desc = optarg;
         break;

         case 't':
	 // printf("t: you entered \"%s\"\n", optarg);
         tagInfo = optarg;
         break;

         case 'c':
	 // printf("c: you entered \"%s\"\n", optarg);

         try {
           chansize = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong chansize parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 'l':
	 // printf("l: you entered \"%s\"\n", optarg);

         try {
           colsize = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong colsize parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 'i':
	 // printf("i: you entered \"%s\"\n", optarg);
         insertionTime = optarg;
         break;

         case 'h':
         printf("Usage: crestCmd create tagMetaInfo [OPTIONS]\n");
         printf("  -f, --file             file with a tag meta info JSON\n");
         printf("  -j, --json             JSON string with a tag meta info\n");
         printf("  -n, --tagName          tag name\n");
         printf("  -d, --description      tag description\n");

         printf("  -t, --tagInfo          tag info\n");
         printf("  -c, --chansize         chansize\n");
         printf("  -l, --colsize          colsize\n");
         printf("  -i, --insertionTime    insertion time\n");

         printf("  -h, --help             print this help and exit\n");
         printf("\n");
         return(0);

         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
   if (optind < argc){
     printf ("ERROR: non-option ARGV-elements: ");
       while (optind < argc) {
       printf ("%s ", argv[optind++]);
     }
     putchar ('\n');
     exit(0);
   }
   //

   /*
   std::cout << "  tagName       = "   << tagName       << std::endl;
   std::cout << "  description   = "   << desc          << std::endl;
   std::cout << "  tagInfo       = "   << tagInfo       << std::endl;
   std::cout << "  chansize      = "   << chansize      << std::endl;
   std::cout << "  colsize       = "   << colsize       << std::endl;
   std::cout << "  insertionTime = "   << insertionTime << std::endl;
   */

   // Required Parameter Checking:
   if (tagName == ""){
     std::cout << "ERROR: please enter the tag name!" << std::endl;
     exit(0);
   }
   else if (desc == ""){
     std::cout << "ERROR: please enter the tag description!" << std::endl;
     exit(0);
   }
   else if (tagInfo == ""){
     std::cout << "ERROR: please enter the tagInfo!" << std::endl;
     exit(0);
   }
   else{
     nlohmann::json js = 
     {
       {"tagName", tagName},
       {"description", desc},
       {"tagInfo", tagInfo}, 
       {"chansize", chansize},
       {"colsize", colsize}
     };

     // optional parameter:
     if (insertionTime != ""){
       js["insertionTime"] = insertionTime;
     }

     try {
       myCrestClient.createTagMetaInfo(js);
       std::cout << "Tag meta info was created." << std::endl;
     }
     catch (const std::exception& e){
      std::cerr << "ERROR: cannot create the tag meta info:" << std::endl;
      std::cerr << e.what() << std::endl; 
     }
     catch(...){
       std::cerr << "ERROR: cannot create the tag meta info" << std::endl; 
    }
   }

   return(0);
}

//

//----------------------

int main(int argc, char* argv[])
{
  std::string path = getCrestPath();
  // std::cout << "Crest Server path = " << path << std::endl;
  CrestClientExt myCrestClient = CrestClientExt(path);

  /*
  std::cout << " argc = " << argc << std::endl;
  for (int i = 0; i < argc; i ++ ){
    std::cout << " i = " << i << "  argv = " << argv[i] << std::endl;

  }
  */

  // if (argc >= 4){
  if (argc >= 3){
    std::string cCommand = argv[1];
    std::string cType = argv[2];

    // std::cout << "COMMAND RUNNING" << std::endl; 

    // GET methods
 
    if (cCommand ==  "get"){
      // std::cout << "get" << std::endl;

      if (cType == "tag"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runGetTag(argc-2, &argv[2],myCrestClient);
      } // tag
      else if (cType == "tagList"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        // runTagList(argc-2, &argv[2],myCrestClient); // old version
        runGTList(argc-2, &argv[2],myCrestClient,cType);
      } // taglist
      else if (cType == "iovList"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runFindAllIovs(argc-2, &argv[2],myCrestClient);
      } // findIovs
      else if (cType == "blob"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runGetBlob(argc-2, &argv[2],myCrestClient);
      } // blob
      else if (cType == "payloadMetaInfo"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runGetPayloadMetaInfo(argc-2, &argv[2],myCrestClient);
      } // payloadMetaInfo
      else if (cType == "payloadTagInfo"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runGetPayloadTagInfo(argc-2, &argv[2],myCrestClient);
      } // payloadTagInfo
      else if (cType == "globalTag"){
	std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runGetGlobalTag(argc-2, &argv[2],myCrestClient);
      } // globalTag
      else if (cType == "globalTagList"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        // runGetGlobalTagList(argc-2, &argv[2],myCrestClient); // old version
        runGTList(argc-2, &argv[2],myCrestClient,cType);
      } // globalTagList
      else if (cType == "globalTagMap"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runGetGlobalTagMap(argc-2, &argv[2],myCrestClient);
      } // get global tag map
      else if (cType == "tagMetaInfo"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runGetTagMetaInfo(argc-2, &argv[2],myCrestClient);
      } // get tag meta info
      else if (cType == "commands"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        printMethods();
      } // commands (command list)
      else{
	  std::cout << "Unknown type: " << cType << std::endl;
      }
    }

    // CREATE methods

    else if (cCommand == "create"){
      // std::cout << "create" << std::endl;

      if (cType == "tag"){
        // std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runCreateTag(argc-2, &argv[2],myCrestClient);
      } // create tag
      else if (cType == "iovAndPayload"){
        // std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runIovAndPayload(argc-2, &argv[2],myCrestClient);
      } // create iov and payload
      else if (cType == "globalTag"){
        // std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runCreateGlobalTag(argc-2, &argv[2],myCrestClient);
      } // create global tag
      else if (cType == "globalTagMap"){
        // std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runCreateGlobalTagMap(argc-2, &argv[2],myCrestClient);
      } // create global tag map
      else if (cType == "tagMetaInfo"){
        // std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runCreateTagMetaInfo(argc-2, &argv[2],myCrestClient);
      } // create tag meta info
      else{
	std::cout << "Unknown type: " << cType << std::endl;
      }
    } // create

    // UPDATE methods:
    else if (cCommand == "update"){
      std::cout << "update" << std::endl;
    } // update


    // REMOVE methods:
    else if (cCommand == "remove"){
      if (cType == "tag"){
        // std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runRemoveTag(argc-2, &argv[2],myCrestClient);
      } // remove tag
      else if (cType == "globalTag"){
        // std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runRemoveGlobalTag(argc-2, &argv[2],myCrestClient);
      } // remove global tag
      else{
	std::cout << "Unknown type: " << cType << std::endl;
      }
    }

    // DUMP methods

    else if (cCommand ==  "dump"){
      // std::cout << "dump" << std::endl;

      if (cType == "tag"){
	// std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runDumpTag(argc-2, &argv[2],myCrestClient);
      } // tag
      else {
        std::cout << "Unknown command: " << cType << std::endl;
      }
    }

    // TEST methods (to use it for tests):
    else if (cCommand == "test"){
      if (cType == "test"){
	std::cout << "test node :" << std::endl;
	std::string str = argv[3];
	std::cout << "argv = " << str << std::endl;
	nlohmann::json js = getArgToJson(str, myCrestClient);
        std::cout << "js = " << std::endl
	      	  << js.dump(4) << std::endl;
      } // test (2)
      /*
      else if (cType == "createTag"){
	std::cout << "Command: " << cCommand << " " << cType << std::endl;
        runCreateTag(argc-2, &argv[2],myCrestClient);
      } // createTag
      */
      else {
        std::cout << "Unknown command: " << cType << std::endl;
      }
      
    }// test (1)
    else{
      std::cout << "Unknown command: " << cCommand << " aaa " << std::endl;
    }

  } 
  else {
    cout << "Wrong crestCmd parameters." << endl;
    cout << "Please, run this program with the parameters:" << endl;
    cout << "crestCmd command type string1 string2" << endl;
  }
  //cout << "Stopped." << endl;
  return 0;
}
